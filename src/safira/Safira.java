/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira;

import gui.Principal;
import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;
import safira.Speech.SpeechRecognition;

/**
 *
 * @author root
 */
public class Safira {

    /**
     * @param args the command line arguments
     */
    
    
    //static Principal principal = null;
    static SpeechRecognition speechRecognition = null;
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        Principal principal = new Principal();
        principal.show();


        
        speechRecognition = new SpeechRecognition();
        speechRecognition.setPrincipal(principal);
        speechRecognition.start();
        
        
        
        
        
    }
    
    public static void playSound(){
        URL url = SpeechRecognition.class.getResource("sound.wav");
        AudioClip audio = Applet.newAudioClip(url);
        audio.play();
    }
    
    public static void Dispose(){
        System.out.println("Finalizando Safira");
        //principal.dispose();
        speechRecognition.Dispose();
        System.exit(0);
    }
    
    
}
