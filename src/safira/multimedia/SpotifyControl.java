/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.multimedia;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.expectit.Expect;
import net.sf.expectit.ExpectBuilder;
import net.sf.expectit.Result;
import static net.sf.expectit.matcher.Matchers.anyString;
import safira.Speech.SpeechRecognition;

/**
 *
 * @author root
 */
public class SpotifyControl {
    
    Expect expect;
    
    public void Initialize(){
        
        Process process;
        try {
            process = Runtime.getRuntime().exec("/bin/sh");

            expect = new ExpectBuilder()
                    .withInputs(process.getInputStream())
                    .withOutput(process.getOutputStream())
                    .withTimeout(1, TimeUnit.SECONDS)
                    .withExceptionOnFailure()
                    .build();

            // try-with-resources is omitted for simplicity
            //expect.sendLine("ls");
            
            // Obtem Diretorio local
            //String url = this.getClass().getResource("").toString();
            //String local[] = new String[3];  
            //local = url.split(":");

           
            
            // Inicia Julius
            //expect.sendLine("cd " + local[1] + "coruja_jlapsapi");
            //expect.sendLine("export ALSADEV=\"plughw:2,0\"");
            //expect.sendLine("sudo julius -input mic -C julius.jconf");
            //expect.sendLine("sudo ls");
            
            // Sai do Root e vai pro usuario douglas
            expect.sendLine("spotify status dbus.service");
            
            //expect.sendLine("bus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause");
            //expect.sendLine("ls");
            System.out.println("Spotify Controller Iniciado");
            //rastreioConsole();
            
            
        }
        catch (IOException ex) {
            Logger.getLogger(SpeechRecognition.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error 1: " + ex);
        }
        
    }
    
    public void PlayStop(){        
        try {
            System.out.println("Executadi PlayStop");
            expect.sendLine("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause");
        } catch (IOException ex) {
            Logger.getLogger(SpotifyControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void playMusic(String musicName){
        try {
            expect.sendLine("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.OpenUri string:spotify:track:0pK7xotcYmRlwnJQUTwi17");
        } catch (IOException ex) {
            Logger.getLogger(SpotifyControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    // Imprimi todos os resultados
    private void rastreioConsole(){
        
            while(true){
            
                try {
                Result juliusReturn;
                
                    juliusReturn = expect.expect(anyString());
                
                
                if(juliusReturn != null){
                    
                    // Quebra resultado
                    String[] valorComSplit = juliusReturn.toString().split("\n");                     
                    // Numera resultado quebrado
                    for (int i = 0; i < valorComSplit.length; i++) {
                        // Imprime
                        System.out.println("Linha "+i+": "+ valorComSplit[i]);
                    }
                    
                    //commandDetected(valorComSplit);
                
                }
                }                
                catch (IOException ex) {
                    //System.out.println("Nada a imprimir");

                }
            
            
            }      
        
    }
    
}
