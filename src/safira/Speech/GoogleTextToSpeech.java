/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.Speech;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author root
 */
public class GoogleTextToSpeech extends Thread {
    
    public void run(){
        initialize();        
    }
    
    public void setspeechRecognitionMonitor(SpeechRecognitionMonitor speechRecognitionMonitor) {
        this.speechRecognitionMonitor = speechRecognitionMonitor;
    }
    
    WebDriver driver;
    SpeechRecognitionMonitor speechRecognitionMonitor;
    
    private void initialize(){
        
        // TODO add your handling code here:
        // Configura Browser
        System.setProperty("webdriver.chrome.driver", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        // Seta para aceitar todas as permisões
        options.addArguments("--use-fake-ui-for-media-stream");
        capabilities.setCapability("chrome.binary", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(capabilities);
        
        // Navega ata página
        driver.get("https://translate.google.com/#pt/en/");
        //Seta Linguagem
        //WebElement idioma = driver.findElement(By.id("select_language"));
        //idioma.sendKeys("Português");  
        
        //speak("");
        
    }
    
    public void speak(String text){
        
            System.out.println("Google Speak Acionado");
            driver.get("https://translate.google.com/#pt/en/" + text);
            //Seta Texto
            //WebElement idioma = driver.findElement(By.id("source"));
            
            try {
            Thread.sleep(1000);
            } catch (InterruptedException ex) {
            Logger.getLogger(GoogleTextToSpeech.class.getName()).log(Level.SEVERE, null, ex);
        }
            //idioma.sendKeys(text);
            // Inicia Speak
            //WebElement webElement2 = driver.findElement(By.id("gt-src-listen"));
            WebElement webElement2 = driver.findElement(By.id("gt-src-listen"));            
            webElement2.sendKeys(Keys.ENTER);
            System.out.println("Clicou");
            
            
            boolean speaking = true;
            
            while (speaking){
                try {                    
                    String teste2 = driver.findElement(By.xpath("//*[@id=\"gt-src-listen\"]")).getAttribute("aria-pressed");
                    //System.out.println("Teste 2: " + teste2);
                    if(teste2.equals("false")){
                        //speechRecognitionMonitor.speechRecognitionResume();
                        speaking = false;
                    }
                    
                }
                catch (Exception exp){
                    System.out.println("Error");
                }
                
                try {
                Thread.sleep(1000);
                } catch (InterruptedException ex) {
                Logger.getLogger(GoogleTextToSpeech.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
            
            //WebElement webElement3 = driver.findElement(By.className(text));
            
        
    }
    
    public void Dispose(){
        driver.quit();
    }
    
}
