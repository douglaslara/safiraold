/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.Speech;

import gui.Principal;
import javax.swing.JTextArea;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import static safira.Safira.playSound;


/**
 *
 * @author root
 */
public class GoogleSpeechRecognition extends Thread {


    
   
    public void run (){
        googleSpeechRecognitionStart();
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }
    
    public void setspeechRecognitionMonitor(SpeechRecognitionMonitor speechRecognitionMonitor) {
        this.speechRecognitionMonitor = speechRecognitionMonitor;
    }
    
    
    // Variaveis
    Principal principal;
    WebDriver driver;
    SpeechRecognitionMonitor speechRecognitionMonitor;
    boolean recognitionStatus = false;
    String recognized = "";
    String recognition = "";
    int count = 0;
    Interpreter interpreter = new Interpreter();
    
    
    private void googleSpeechRecognitionStart(){        
                
        // TODO add your handling code here:
        // Configura Browser
        System.setProperty("webdriver.chrome.driver", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        // Seta para aceitar todas as permisões
        options.addArguments("--use-fake-ui-for-media-stream");
        capabilities.setCapability("chrome.binary", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(capabilities);
        
        // Navega ata página
        driver.get("https://www.google.com/intl/pt/chrome/demos/speech.html");
        //Seta Linguagem
        WebElement idioma = driver.findElement(By.id("select_language"));
        idioma.sendKeys("Português");  
        
        
        interpreter.setspeechRecognitionMonitor(speechRecognitionMonitor);
        interpreter.start();
        
        
    }
    
    public void googleRecognize(){   
        
        // Inicia voice Recognition
        WebElement webElement = driver.findElement(By.id("start_button"));
        webElement.click();
        recognitionStatus = true;
        
        
        
        
        //googleSpeechRecognitionResult.status = true;
        
        get();    
       
    }
    
    public void get(){  
        
        int countmax = 25;
        
        while(recognitionStatus){
            
            String recognizedLocal = null;
            String recognitionLocal = null;
            
        try{
            recognizedLocal = driver.findElement(By.id("final_span")).getText();
            recognitionLocal = driver.findElement(By.id("interim_span")).getText();
            
            //System.out.println(TxtBoxContent.getText() + "("+TxtBoxContent2.getText()+"...)");
            
        }
        catch(Exception ex){System.out.println("Error no get 1: " + ex);
        }
        count++;
        
        if (count < countmax) {
            //recognized = recognizedLocal;
            //recognition = recognitionLocal;
            System.out.println("count é: " + count);
            System.out.println("recognizedLocal: " + recognizedLocal);
            System.out.println("recognized: " + recognized);
            System.out.println("recognitionLocal: " + recognitionLocal);
            System.out.println("recognition: " + recognition);
            
            if(!recognition.equalsIgnoreCase(recognitionLocal)){
                if(!recognitionLocal.equals("")){
                    recognized = recognizedLocal;
                    recognition = recognitionLocal;
                    countmax = 5;
                    count = 0;}
                    
                }
        }
            
                                
            
            /*
            
            if (!recognition.equals(recognitionLocal) || !recognized.equals(recognizedLocal))  {
                System.out.println("Oi1");
                if(!recognitionLocal.equals("") || !recognizedLocal.equals("")){
                    
                System.out.println("Laço 1");
                recognized = recognizedLocal;
                recognition = recognitionLocal;
                
                //principal.atualiza(recognized + "("+recognition+"...)");
                count = 0;}
                else{
                    
                count++;}
            }
            else{
                //System.out.println("Laço 2");
                count++;
            }
           
        }*/
        else {
            //System.out.println("PESQUISAR!");
            // Finaliza Voice Recognition
            WebElement webElement = driver.findElement(By.id("start_button"));
            webElement.click();
            recognitionStatus = false;
            count = 0;
            
            recognized = recognizedLocal;
            recognition = recognitionLocal;
  
            if(recognized.equals("") && recognition.equals("")){
                playSound();
                interpreter.interpretByGoogle("Não entendi!");         
            }
            else{
                playSound(); 
                interpreter.interpretByGoogle(recognized + recognition);
                recognized = "";
                recognition = "";
                               
            }
            //break;
        }
        //jText.setText(TxtBoxContent.getText() + "("+TxtBoxContent2.getText()+"...)");
        
        //if (TxtBoxContent.getText() == ""){
        //    
        //}
        
        //System.out.println("Oi");
        //System.out.println(TxtBoxContent.getText() + "("+TxtBoxContent2.getText()+"...)");
            
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                System.out.println("Error no GET" + ex);

            }
        }
    }
    
    public void Dispose(){
        driver.quit();
        interpreter.Dispose();
    }
    
    
    
    
}
