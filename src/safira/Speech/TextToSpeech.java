/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.Speech;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 *
 * @author root
 */
public class TextToSpeech extends Thread{
    
    public void run(){
        initialize();
    }
    
    WebDriver driver;
    
    private void initialize(){
        
        // TODO add your handling code here:
        // Configura Browser
        System.setProperty("webdriver.chrome.driver", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        // Seta para aceitar todas as permisões
        options.addArguments("--use-fake-ui-for-media-stream");
        capabilities.setCapability("chrome.binary", "/home/douglas/NetBeansProjects/Teste Selenium/src/teste/selenium/chromedriver");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(capabilities);
        
        // Navega ata página
        driver.get("http://www.oddcast.com/home/demos/tts/tts_example.php");
        //Seta Linguagem
        WebElement idioma = driver.findElement(By.id("lang"));
        idioma.sendKeys("Portuguese");  
        
        //Seta Voz
        WebElement voice = driver.findElement(By.id("voice"));
        voice.sendKeys("Gabriela (Brazilian)"); 
        
        //speak("");
        
    }
    
    public void speak(String text){
        System.out.println("Speak acionado");
        //Seta Texto
        WebElement texto = driver.findElement(By.id("textToSay"));
        texto.sendKeys(text); 
        //driver.get("https://translate.google.com/#pt/en/" + text);
        // Inicia Speak
        WebElement webElement = driver.findElement(By.name("SayButton"));
        webElement.click();
    }
    
}
