/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.Speech;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.util.logging.Level;
import java.util.logging.Logger;
import safira.multimedia.ArduinoControl;
import safira.multimedia.SpotifyControl;

/**
 *
 * @author root
 */
public class Interpreter extends Thread {
    
    public void run(){
        initilize();
    }
    
    public void setspeechRecognitionMonitor(SpeechRecognitionMonitor speechRecognitionMonitor) {
        this.speechRecognitionMonitor = speechRecognitionMonitor;
    }
    
    // Variaveis
    public String recognized;
    public String recognition;
    public WebClient webclient;
    private HtmlPage page;
    GoogleTextToSpeech textToSpeech = new GoogleTextToSpeech();
    SpotifyControl spotify;
    SpeechRecognitionMonitor speechRecognitionMonitor;
    ArduinoControl arduinoControl;
    
    
    private void initilize(){
        
        // Ignora alguns erros
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        BrowserVersion version = BrowserVersion.CHROME;
        //version.setUserAgent("Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5");
        webclient = new WebClient(version);
        // Ignora erros JavaScript
        webclient.getOptions().setThrowExceptionOnScriptError(false);
        Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF); 


        System.out.println("Browser HTMLUnit construido");

        textToSpeech.setspeechRecognitionMonitor(speechRecognitionMonitor);
        textToSpeech.start();
        
        spotify = new SpotifyControl();
        spotify.Initialize();
        
        arduinoControl = new ArduinoControl();
        arduinoControl.start();
        

    }
    
    public void interpretByGoogle(String recognized)       {  
        
        try {
            if(recognized.equals("Não entendi!")){
                textToSpeech.speak("Não entendi!");
            }
            else if(recognized.equals("Play") || recognized.equals("Continuar música") || recognized.equals("Parar música") || recognized.equals("Stop")){
                System.out.println("Player");
                spotify.PlayStop();
                //speechRecognitionMonitor.speechRecognitionResume();
            }
            else if(recognized.contains("Tocar")){
                System.out.println("Player tocar:");
                //spotify.playMusic;
                //speechRecognitionMonitor.speechRecognitionResume();
            }            
            else if(recognized.equalsIgnoreCase("Ligar a luz") || recognized.equalsIgnoreCase("Acender a luz")){
                arduinoControl.ligarLuz();
                textToSpeech.speak("Luz quarto: Douglas ligada.");                
                //speechRecognitionMonitor.speechRecognitionResume();
            }
            else if(recognized.equalsIgnoreCase("Apagar a luz") || recognized.equalsIgnoreCase("Desligar a luz")){
                arduinoControl.apagarLuz();
                textToSpeech.speak("Luz quarto: Douglas apagada.");
//textToSpeech.speak("Sim. Nublado com chuva de manhã. Aberturas de sol à  tarde e pancadas de chuva. Noite nublada com possibilidade de garoa.");
                //speechRecognitionMonitor.speechRecognitionResume();
                
            }
            else if(recognized.equalsIgnoreCase("Lavar a louça")){
                textToSpeech.speak("Não quero.");
                //speechRecognitionMonitor.speechRecognitionResume();
            }
            
            else{
            
            if(recognized.equalsIgnoreCase("Vai chover hoje")){
                ///// Tempo
                System.out.println("Tempo");
                DomElement rx = page.getFirstByXPath("//*[@id=\"wob_dc\"]");
                textToSpeech.speak(rx.asText());
                return;
                //speechRecognitionMonitor.speechRecognitionResume();
            }
            // Busca
            page = webclient.getPage("https://www.google.com.br/search?q=" + recognized);
            
            // Verifica se é uma fonte externa
            if(page.getFirstByXPath("//*[@id=\"rso\"]/li/div[1]/div/div[1]/ol/div[3]/div") != null){
                //System.out.println("Talvez seja uma fonte externa");
                
                DomElement rx1 = page.getFirstByXPath("//*[@id=\"rso\"]/li/div[1]/div/div[1]/ol/div[3]/div");
                if (rx1.asText().contains(".com") || rx1.asText().contains("/.")){
                    System.out.println("é uma fonte externa");
                    // se for, pega resultado do Wiki, lado direto
                    if(page.getFirstByXPath("//*[@id=\"rhs_block\"]/ol/li/div[1]/div/div[1]/ol/div[3]/div/div/div") != null ){
                        DomElement rx = page.getFirstByXPath("//*[@id=\"rhs_block\"]/ol/li/div[1]/div/div[1]/ol/div[3]/div/div/div");
                        //System.out.println("Vai falar: " + rx.asText());
                        textToSpeech.speak(rx.asText());
                    }
                    else{
                        System.out.println("Nenhum resultado encontrado");
                    }
                     
                }
                else{
                    textToSpeech.speak(rx1.asText());
                }   
            }
            // Se não for uma fonte externa pega o primeiro resultado
            else{
            DomElement rx2 = page.getFirstByXPath("//*[@id=\"rso\"]/li/div[1]/div/div[1]/ol/div[2]");            
            if(rx2 != null){            
                textToSpeech.speak(rx2.asText());
                return;
            }
            else if(page.getFirstByXPath("//*[@id=\"rso\"]/li/div[1]/div/div[1]/ol/div") != null){
                System.out.println("Laço 22");
                DomElement rx = page.getFirstByXPath("//*[@id=\"rso\"]/li/div[1]/div/div[1]/ol/div");
                textToSpeech.speak(rx.asText());
                System.out.println("Resultado" + rx.asText());
                return;
            }
            // Se for Tempo
            else if(page.getFirstByXPath("//*[@id=\"wob_dc\"]") != null){
                System.out.println("Tempo");
                DomElement rx = page.getFirstByXPath("//*[@id=\"wob_dc\"]");
                textToSpeech.speak(rx.asText());
                return;
            }
            // Se for Horas
            else if(page.getFirstByXPath("//*[@id=\"rso\"]/div[1]/div/div/div[1]") != null )
                System.out.println("pode ser Horas");
                DomElement rx = page.getFirstByXPath("//*[@id=\"rso\"]/div[1]/div/div/div[1]");
                
                if(rx.asText().contains(".com") || rx.asText().contains("http")){
                             System.out.println("Não é horas");
                             textToSpeech.speak("Nenhum resultado encontrado.");}
                else                    
                    textToSpeech.speak(rx.asText());
            }
            
            
            
            
            
            
            /* Antigo Metodo
            DomElement rx1 = page.getFirstByXPath("//*[@id=\"ires\"]/ol/li[1]/div");
            String[] rx1Result = rx1.asText().split("\n");
            
            if(rx1Result[0].contains("http")){
                textToSpeech.speak("Nenhum resultado encontrado.");
            }
            else if (rx1Result[0].contains("Clima em")){
                page = webclient.getPage("http://www.climatempo.com.br/previsao-do-tempo/cidade/271/curitiba-pr");
                
                DomElement rx2 = page.getFirstByXPath("/html/body/div[4]/div[1]/div[6]/div/div[2]/div/div[1]/div/div[5]/p");
                String[] rx1Result2 = rx1.asText().split("\n");
                
                textToSpeech.speak(rx1Result[0]);
            }
            else{
                textToSpeech.speak(rx1Result[0]);
            }*/
            
            
            }
            
            
        } catch (Exception ex) {
            System.out.println("Erro aqui");Logger.getLogger(Interpreter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void Dispose(){
        textToSpeech.Dispose();
        //arduinoControl.destroy();
    }
    
}
