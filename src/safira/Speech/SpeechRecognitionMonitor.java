/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safira.Speech;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.expectit.Expect;
import net.sf.expectit.ExpectBuilder;
import net.sf.expectit.Result;
import static net.sf.expectit.matcher.Matchers.anyString;
import static safira.Safira.playSound;

/**
 *
 * @author root
 */
public class SpeechRecognitionMonitor extends Thread {
    
    public void run(){
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(SpeechRecognitionMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Iniciar!!!");
        
        initialize();
    }
    
    // Variaveis
    Expect expect2;
    Process process2;
    GoogleSpeechRecognition googleSpeechRecognition = new GoogleSpeechRecognition();
    
    private void initialize(){
        
        
        googleSpeechRecognition.start();
        googleSpeechRecognition.setspeechRecognitionMonitor(this);
        
        try {
            process2 = Runtime.getRuntime().exec("/bin/sh");
            
            expect2 = new ExpectBuilder()
                    .withInputs(process2.getInputStream())
                    .withOutput(process2.getOutputStream())
                    .withTimeout(1, TimeUnit.SECONDS)
                    .withExceptionOnFailure()
                    .build();
            
            // Obtem Diretorio local
            String url = this.getClass().getResource("").toString();
            String local[] = new String[3];  
            local = url.split(":");
            
            // Inicializa JControl
            expect2.sendLine("cd " + local[1] + "coruja_jlapsapi");
            expect2.sendLine("sudo jcontrol localhost 10501");
            
            System.out.println("JControl Inicializado");
            rastreioConsole();
            
            
        } catch (IOException ex) {
            Logger.getLogger(SpeechRecognitionMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    // Imprimi todos os resultados
    private void rastreioConsole(){
            while(true){
            
                try {
                Result juliusReturn;
                
                    juliusReturn = expect2.expect(anyString());
                
                
                if(juliusReturn != null){
                    
                    // Quebra resultado
                    String[] valorComSplit = juliusReturn.toString().split("\n");                     
                    // Numera resultado quebrado
                    for (int i = 0; i < valorComSplit.length; i++) {
                        // Imprime
                        System.out.println("Linha "+i+": "+ valorComSplit[i]);
                    }
                    
                    
                    
                    commandDetected(valorComSplit);
                
                }
                }                
                catch (IOException ex) {
                    //System.out.println("Nada a imprimir");

                }
            
            
            }      
        
    }
    
    
    
    
    
    private void commandDetected(String [] valorComSplit){
        
        try {
            
        // Detectado
        String palavra = null;
        double[] cmscore = new double[3];
        float coefViterbi = 0;
        
        // Requesitos
        double nivelConf = 0.70;
        double coefViterbiMin = -140000000;
        String palavra2 = "oksafira";
        
        
        if(valorComSplit[0].contains("<RECOGOUT>")){
        
        
        
        if(valorComSplit[3].contains("<WHYPO WORD=")){
                String[] valorComSplit2 = valorComSplit[3].split("\"");
                //System.out.println("Palavra : " +valorComSplit2[1]);
                
                palavra = valorComSplit2[1];
                if (!palavra.equals("")){
                    if(valorComSplit[2].contains("CLASSID=\"<s>\"")){
                    String[] valorComSplit3 = valorComSplit[2].split("\"");
                    String[] valorComSplit4 = valorComSplit[3].split("\"");
                    String[] valorComSplit5 = valorComSplit[4].split("\"");
                    //System.out.println("Score: " +valorComSplit3[7] + ", " + valorComSplit4[7] + ", " + valorComSplit5[7]);
                    cmscore[0] = Double.parseDouble(valorComSplit3[7]);
                    cmscore[1] = Double.parseDouble(valorComSplit4[7]);
                    cmscore[2] = Double.parseDouble(valorComSplit5[7]);
                    }
                    
                    if(valorComSplit[1].contains("SCORE=")){
                    String[] valorComSplit6 = valorComSplit[1].split("\"");
                    coefViterbi = Float.parseFloat(valorComSplit6[3]);
                    //System.out.println("coefViterbi: " +valorComSplit6[3]);
                    }   
                    
                    System.out.println("Palavra: " + palavra + "\nScore: " + cmscore[0] + " " + cmscore[1] + " " + cmscore[2] + "\nCoeficiente de Viterbi: " + coefViterbi);
                }
        }
        }
        
        /*
            for (int i = 0; i < valorComSplit.length; i++) {
                if(valorComSplit[i].contains("SCORE=")){
                String[] valorComSplit2 = valorComSplit[i].split("\"");
                System.out.println("Score, linha: " + i + " Resultado: " +valorComSplit2[3]);
                }
                if(valorComSplit[i].contains("CLASSID=\"<s>\"")){
                String[] valorComSplit2 = valorComSplit[i].split("\"");
                System.out.println("nivelConf, linha: " + i + " Resultado: " +valorComSplit2[7]);
                }
            }
            System.out.println("Verificou todas as linhas!");
            
            
            /*
            for (int i = 0; i < 10; i++) {
                
            }
        
        
        if (valorComSplit == null){
            System.out.println("O Array valorComSplit é nulo");
        }
        
        if(valorComSplit.length < 1){
            System.out.println("Não foi uma palavra detectada");
        }
        else{
        
        // Localiza palavra detectada
         if(valorComSplit[6].toLowerCase().contains("SCORE=")){
         String[] valorComSplit2 = valorComSplit[6].split("\"");
         System.out.println(valorComSplit2[0]);
         
         if(valorComSplit2.length != 1){             
             palavra = valorComSplit2[1]; 
             System.out.println("Palavra: " + palavra);
         
         
         
         
         // Localiza score
         if(valorComSplit[9].toLowerCase().contains("cmscore1:")){
         String[] valorComSplit3 = valorComSplit[9].split(" ");
         cmscore[0] = Double.parseDouble(valorComSplit3[1]);
         cmscore[1] = Double.parseDouble(valorComSplit3[2]);
         cmscore[2] = Double.parseDouble(valorComSplit3[3]);
         System.out.println("cmScore: " + cmscore[0] + " " + cmscore[1] + " " + cmscore[2]);                 
         }
                    
         // Localiza coeficiente de Viterbi
         if(valorComSplit[10].toLowerCase().contains("score1:")){
                        
         String[] valorComSplit4 = valorComSplit[10].split(" ");
         coefViterbi = Float.parseFloat(valorComSplit4[1]);
         //System.out.println("coeficiente de Viterbi: " + coefViterbi);
         //for (int i = 0; i < valorComSplit4.length; i++) {
                        //    System.out.println(i + " OIIII " + valorComSplit4[i]);
                        //}
                        System.out.println("Viterbi: " + coefViterbi);
                    }
         }
         
         
                    
        */
         // Verifica se foi detectado alguma palavra
         if (palavra != null && coefViterbi != 0){
                    if(!palavra.contains(palavra2)){
                        System.out.println("Não é o comando de ativação, resultado : " + palavra);
                    }             
                    else if(cmscore[0] < nivelConf || cmscore[1] < nivelConf || cmscore[2] < nivelConf){
                        System.out.println("Nivel de confiança insuficiente");
                        
                    }                    
                    else if (coefViterbi < coefViterbiMin){
                        System.out.println("Nivel de confiança do Viterbi insuficiente");                        
                    }
                    else {  
                        playSound();
                        System.out.println("Score: " +cmscore[0] + ", " + cmscore[1] + ", "+ cmscore[2]);
                        System.out.println("Passou...");
                        //expect2.sendLine("pause");
                        
                        googleSpeechRecognition.googleRecognize();
                        
                    }

                    
         }
         
         
        
        }
        catch(Exception ex){
            System.out.println("Error 2: " + ex + "Localizado em: " + ex.getLocalizedMessage());
        }
        
    }
    
    public void speechRecognitionResume(){
        try {
            expect2.sendLine("resume");
            System.out.println("Resume");
        } catch (IOException ex) {
            Logger.getLogger(SpeechRecognitionMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void Dispose(){
        try {
            expect2.close();
            googleSpeechRecognition.Dispose();
        } catch (IOException ex) {
            Logger.getLogger(SpeechRecognitionMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
